#include "global.hpp"
#include "scanner.hpp"
#include "parser.hpp"
#include <algorithm>

struct InsItem {
    const char *name;
    OpType op;
    bool operator < (const InsItem &rhs) const {
        return strcmp(name, rhs.name) < 0;
    }
    bool operator < (const char *rhs) const {
        return strcmp(name, rhs) < 0;
    }
} insTable[] = {
    {"abs", OP_ABS},   {"add", OP_ADD},     {"addi", OP_ADDI},
    {"addu", OP_ADDU}, {"addiu", OP_ADDIU}, {"and", OP_AND},
    {"andi", OP_ANDI}, {"div", OP_DIV},     {"divu", OP_DIVU},
    {"mul", OP_MUL},   {"mulo", OP_MULO},   {"mulou", OP_MULOU},
    {"mult", OP_MULT}, {"multu", OP_MULTU}, {"neg", OP_NEG},
    {"negu", OP_NEGU}, {"nor", OP_NOR},     {"not", OP_NOT},
    {"or", OP_OR},     {"ori", OP_ORI},     {"rem", OP_REM},
    {"remu", OP_REMU}, {"rol", OP_ROL},     {"ror", OP_ROR},
    {"sll", OP_SLL},   {"sllv", OP_SLLV},   {"sra", OP_SRA},
    {"srav", OP_SRAV}, {"srl", OP_SRL},     {"srlv", OP_SRLV},
    {"sub", OP_SUB},   {"subu", OP_SUBU},   {"xor", OP_XOR},
    {"xori", OP_XORI}, {"li", OP_LI},       {"lui", OP_LUI},
    {"seq", OP_SEQ},   {"sge", OP_SGE},     {"sgeu", OP_SGEU},
    {"sgt", OP_SGT},   {"sgtu", OP_SGTU},   {"sle", OP_SLE},
    {"sleu", OP_SLEU}, {"slt", OP_SLT},     {"slti", OP_SLTI},
    {"sltu", OP_SLTU}, {"sltiu", OP_SLTIU}, {"sne", OP_SNE},
    {"b", OP_B},       {"beq", OP_BEQ},     {"beqz", OP_BEQZ},
    {"bge", OP_BGE},   {"bgeu", OP_BGEU},   {"bgez", OP_BGEZ},
    {"bgt", OP_BGT},   {"bgtu", OP_BGTU},   {"bgezal", OP_BGEZAL},
    {"bgtz", OP_BGTZ}, {"ble", OP_BLE},     {"bleu", OP_BLEU},
    {"blez", OP_BLEZ}, {"bgezal", OP_BGEZAL}, {"bltzal", OP_BLTZAL},
    {"blt", OP_BLT},   {"bltu", OP_BLTU},   {"bltz", OP_BLTZ},
    {"bne", OP_BNE},   {"bnez", OP_BNEZ},   {"j", OP_J},
    {"jal", OP_JAL},   {"jalr", OP_JALR},   {"jr", OP_JR},
    {"la", OP_LA},     {"lb", OP_LB},       {"lbu", OP_LBU},
    {"ld", OP_LD},     {"lh", OP_LH},       {"lhu", OP_LHU},
    {"lw", OP_LW},     {"sb", OP_SB},       {"sd", OP_SD},
    {"sh", OP_SH},     {"sw", OP_SW},       {"move", OP_MOVE},
    {"mfhi", OP_MFHI}, {"mflo", OP_MFLO},   {"nop", OP_NOP},
    {"syscall", OP_SYSCALL},
};
const int insSize = sizeof(insTable) / sizeof(insTable[0]);

Parser::Parser(const char *filename) : scanner(filename) {
    // build instruction table
    std::sort(insTable, insTable + insSize);
    parse();
}

inline bool strequ(const char *a, const char *b) { return strcmp(a, b) == 0; }

// functon alias
#define getToken()  scanner.getToken()
#define getStrVal() scanner.getStrVal()
#define getIntVal() scanner.getIntVal()
#define match(x, s) scanner.match(x, s)

char *copyString(const char *str) {
    char *t = new char[strlen(str) + 1];
    strcpy(t, str);
    return t;
}

void Parser::printIns(Instruction &ins) {
    for (int i = 0; i < insSize; i++)
        if (insTable[i].op == ins.cmd) {
            printf("%8s %3d %3d %3d ",insTable[i].name, 
                    ins.op[0], ins.op[1], ins.op[2]);
            cout << ins.label << endl;
            return;
        }
    printf("invalid instruction\n");
}

void Parser::parse() {
    BlockType nowType = LABEL_TEXT;
    token = getToken();
    while (token != TK_EOF) {
        //cout << token2str[token] << "  ";
        bool getnext = true;
        if (token == TK_ASMDIR) {  // assemble directives
            if (strequ(getStrVal(), "data"))
                nowType = LABEL_DATA;
            else if (strequ(getStrVal(), "text"))
                nowType = LABEL_TEXT;
            else if (strequ(getStrVal(), "word") || strequ(getStrVal(), "half")
                  || strequ(getStrVal(), "byte")) {
                vector<int> tmp;

                int size;
                if (getStrVal()[0] == 'w')      size = 4;
                else if (getStrVal()[1] == 'h') size = 2;
                else                            size = 1;

                // read constant list
                do {
                    match(TK_NUM, "error in word?\n");
                    tmp.push_back(getIntVal());
                } while ((token = getToken()) == TK_COMMA);
                unsigned char *buf = new unsigned char [tmp.size() * size];

                // copy to data
                for (size_t i = 0; i < tmp.size(); i++) {
                    //*((int *)(buf + i * size)) = tmp[i];
                    memcpy(buf + i * size, &tmp[i], sizeof(char) * size);
                }
                data.push_back(DataBlock{buf, tmp.size() * size});
                getnext = false;
            } else if (strequ(getStrVal(), "space")) {
                printf("error in .space\n");
            } else if (strequ(getStrVal(), "align")) {
                match(TK_NUM, "error in align\n");
                data.push_back(DataBlock{(unsigned char*)-1, // use -1
                                            (size_t)getIntVal()});
            } else if (strequ(getStrVal(), "ascii") || 
                       strequ(getStrVal(), "asciiz")) {
                size_t add = strequ(getStrVal(), "asciiz") ? 1 : 0;
                match(TK_STRING, "error in ascii\n");
                data.push_back(DataBlock{(unsigned char *)copyString(
                            getStrVal()), strlen(getStrVal()) + add});
            } else printf("Error!\n");
        } else if (token == TK_LABEL) {           // label
            if (nowType == LABEL_TEXT)
                labelTable[getStrVal()] = Label{LABEL_TEXT, text.size()};
            else if (nowType == LABEL_DATA)
                labelTable[getStrVal()] = Label{LABEL_DATA, data.size()};
        } else if (token == TK_ID) {
            text.push_back(getCommand());
            getnext = false;
        } else {
            printf("error in line beginning: %s\n", getStrVal());
        }

        if (getnext) token = getToken();
    }
}

Instruction Parser::getCommand() {
    Instruction ret = {OP_NOP, {0, 0, 0}, 0, ""};

    InsItem *iter = std::lower_bound(insTable, insTable + insSize,
                                                getStrVal());
    if (iter != insTable + insSize && strequ(iter->name, getStrVal())) 
        ret.cmd = iter->op;
    else
        printf("error in getCommand %s\n", getStrVal());

    //printf("parse %s\n", iter->name);

    if (ret.cmd == OP_SYSCALL || ret.cmd == OP_NOP) {
        token = getToken();
        return ret;
    }

    int ct = 0;
    do {
        token = getToken();
        if (token == TK_REG) {    // $to
            ret.op[ct++] = getIntVal(); ret.regNum++; token = getToken();
        } else if (token == TK_ID) { // func
            ret.label = getStrVal(); token = getToken();
        } else if (token == TK_NUM) {  // -4 -3($fp)
            ret.op[ct++] = getIntVal();
            token = getToken();
            if (token == TK_RB_L) {// -3($fp)
                token = getToken();
                if (token == TK_REG)
                    ret.op[ct++] = getIntVal();
                else if (token == TK_ID)
                    ret.label = string(getStrVal());
                match(TK_RB_R, "error in brace match");
                token = getToken();
            }
        } else { // no arguments , such as syscall, nop
            printf("error in getCommand\n");
        }
    } while (token == TK_COMMA);

    return ret;
}

Parser::~Parser() {
    for (size_t i = 0; i < data.size(); i++)
        if (data[i].val != (unsigned char *)-1 && data[i].val != NULL)
            delete [] data[i].val;
}

void Parser::printSelf() {
    puts("=== label ===");
    for (auto iter = labelTable.begin(); iter != labelTable.end(); iter++)
        cout << iter->first << " : " << iter->second.offset << endl;

    puts("=== instruction ===");
    for (size_t i = 0; i < text.size(); i++) {
        printIns(text[i]);
    }

    puts("=== data ===");
    for (size_t i = 0; i < data.size(); i++)
        if (data[i].val != (unsigned char *)-1)
            printf("%3lu %s\n", data[i].len, data[i].val);
}

void parserTest(const char *filename) {
    cout << "..Parser Test Begin.." << endl;
    Parser parser(filename);
    parser.printSelf();
    cout << "..Parser Test End.." << endl;
}
