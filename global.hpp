#ifndef __GLOBAL_HPP__
#define __GLOBAL_HPP__

#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <string>

#include <cstring>
#include <climits>

using std::cout; using std::cin; using std::endl;
using std::map; using std::vector; using std::string;

#define REG_NUM    32
#define STACK_TOP  0xFFF

typedef enum {
    // arithmetic and logic
    OP_ABS, OP_ADD, OP_ADDI, OP_ADDU, OP_ADDIU, OP_AND, OP_ANDI,
    OP_DIV, OP_DIV2, OP_DIVU, OP_MUL, OP_MULO, OP_MULOU, OP_MULT, OP_MULTU,
    OP_NEG, OP_NEGU, OP_NOR, OP_NOT, OP_OR, OP_ORI, OP_REM, OP_REMU,
    OP_ROL, OP_ROR,
    OP_SLL, OP_SLLV, OP_SRA, OP_SRAV, OP_SRL, OP_SRLV, OP_SUB,
    OP_SUBU, OP_XOR, OP_XORI,
    // immediate
    OP_LI, OP_LUI, OP_SEQ, OP_SGE, OP_SGEU, OP_SGT, OP_SGTU,
    OP_SLE, OP_SLEU, OP_SLT, OP_SLTI, OP_SLTU, OP_SLTIU, OP_SNE,
    // branch
    OP_B, OP_BEQ, OP_BEQZ, OP_BGE, OP_BGEU, OP_BGEZ, OP_BGEZAL,
    OP_BGT, OP_BGTU, OP_BGTZ, OP_BLE,
    OP_BLEU, OP_BLEZ, OP_BLTZAL, OP_BLT, OP_BLTU, OP_BLTZ,
    OP_BNE, OP_BNEZ, OP_J, OP_JAL, OP_JALR, OP_JR,
    // load store
    OP_LA, OP_LB, OP_LBU, OP_LD, OP_LH, OP_LHU, OP_LW,
    OP_SB, OP_SD, OP_SH, OP_SW,
    // other
    OP_MOVE, OP_MFHI, OP_MFLO, OP_NOP, OP_SYSCALL,
}OpType;

#define INS_NUM    ((int)(OP_SYSCALL) + 1)

#endif
