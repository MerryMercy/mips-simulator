#include <string>
#include "global.hpp"
#include "simulator.hpp"

inline bool strequ(const char *a, const char *b) {return strcmp(a, b)==0;}

int main(int argc, char *argv[]) {
    const char *file = "first.s";

    bool debugInfo  = false;
    bool statsInfo = false;
    bool parser = false;

    for (int i = 1; i < argc; i++) {
        if (strequ(argv[i] + strlen(argv[i]) - 2, ".s")) {
            file = argv[i];
        } else if (strequ(argv[i], "-parser")) {
            parser = true;
        } else if (strequ(argv[i], "-d")) {
            debugInfo = true;
        } else if (strequ(argv[i], "-s")) {
            statsInfo = true;
        }
    }

    if (parser) {
        parserTest(file);
    } else {
        simulatorTest(file, debugInfo, statsInfo);
    }
    
    return 0;
}

