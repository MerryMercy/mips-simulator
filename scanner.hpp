#ifndef __SCANNER_HPP__
#define __SCANNER_HPP__

#include <fstream>
#include <cctype>

#define BUFFER_SIZE 1024

typedef enum {
    TK_ID, TK_LABEL, TK_ASMDIR, TK_NUM, TK_STRING, TK_REG, 
    TK_COMMA, TK_RB_L, TK_RB_R, TK_SB_L, TK_SB_R,
    TK_EOF, TK_ERROR,
} TokenType;

extern const char *token2str[];

class Scanner{
public:
    Scanner(const char *filename);
    ~Scanner();

    TokenType getToken();
    int getIntVal();
    char *getStrVal();
    bool match(TokenType token, const char *errorMsg);

private:
    int ival; int sign;
    char sval[BUFFER_SIZE]; int slen;

    char *src; int len;
    int pos;

    struct RegName {
        const char *name;
        int no;
        bool operator < (const RegName &rhs) const {
            return strcmp(name, rhs.name) < 0;
        }
        bool operator < (const char *rhs) const {
            return strcmp(name, rhs) < 0;
        }
    } regTable[REG_NUM];

    void getWord();
};

#endif
