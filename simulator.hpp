#ifndef __SIMULATOR_HPP__
#define __SIMULATOR_HPP__

#include "global.hpp"
#include "parser.hpp"

typedef enum {
    STAGE_IF, STAGE_ID, STAGE_EX, STAGE_MEM, STAGE_WB
} Stage;


struct Pipeline {
    Stage stage;
    Instruction *ins;
    size_t pc;
    int res;
    bool operator < (const Pipeline &rhs) const {
        return (int)stage > (int)rhs.stage;
    }
};

#define STAGE_NUM 5

class Simulator {
public:
    Simulator(const char *filename, bool debugInfo, bool statsInfo);
    ~Simulator();
    void run();

private:
    // utilies
    void syscall();

    // run
    void doCycle();
    void doInstruction(Instruction &ins);
    void buildDataSegment();
    void patchLabel();
    void buildDoInstruction();

    // info from parser
    Parser parser;
    vector<Instruction> &text;
    vector<DataBlock>   &rawData;
    map<string, Label>  &labelTable;

    // environment variable
    unsigned char *mem;
    size_t heapTop;
    int reg[REG_NUM + 2];
    bool halted, jumped;
    size_t pc;

    // pipeline
    Instruction *stageIns[STAGE_NUM];
    int regLock[REG_NUM + 2];
    bool memLock;
    Instruction nop;
    bool jumpIn;
    int resID[3], resEX[3], resMEM[3];

    // stats
    int cycleCounter, insCounter;
    int loadCounter, storeCounter;
    int arithCounter, jumpCounter;

    // every instruciton item
    struct DoInstruction {
        bool (* doStage[STAGE_NUM]) (Instruction &ins, Simulator &sm);
        bool isJump;
    } insItem[INS_NUM];

    // print config
    bool printDebug, printStats;
};

void simulatorTest(const char *filename, bool debugInf, bool statsInfo);

#endif
