CC = g++
CFLAGS = -Wall -std=c++11 -O2

_OBJ = scanner.o parser.o main.o simulator.o
_DEPS = global.hpp scanner.hpp parser.hpp simulator.hpp

ODIR = obj
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))

IDIR = .
DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))

$(ODIR)/%.o: %.cc $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

mips: $(OBJ)
	$(CC) -o bin/mips $^ $(CFLAGS)

.PHONY: clean all rebuild

clean:
	rm -f $(ODIR)/*
	rm -f bin/*

all: mips

rebuild: clean all

