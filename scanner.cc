#include <iostream>
#include "global.hpp"
#include "scanner.hpp"
#include <algorithm>


const char *token2str[] = {
    "id", "label", "asm", "number", "string",  "$", ",", "(", ")", "[", "]",
    "EOF", "ERROR"
};

const char *regName[REG_NUM] = {
    "zero", "at", "v0", "v1", "a0", "a1", "a2", "a3",
    "t0", "t1", "t2", "t3", "t4", "t5", "t6", "t7",
    "s0", "s1", "s2", "s3", "s4", "s5", "s6", "s7",
    "t8", "t9", "k0", "k1", "gp", "sp", "fp", "ra"};


Scanner::Scanner(const char *filename) {
    std::ifstream fin(filename, std::ios_base::binary);

    // get size
    fin.seekg(0, std::ios_base::end);
    len = fin.tellg();
    src = new char[len + 1];

    len--;

    // read all
    fin.seekg(0, std::ios_base::beg);
    fin.read(src, len);
    src[len + 1] = '\0';

    // init pos 
    pos = 0; ival = 0; sval[0] = '\0';

    // init register table
    for (int i = 0; i < REG_NUM; i++) {
        regTable[i].name = regName[i];
        regTable[i].no   = i;
    }
    std::sort(regTable, regTable + REG_NUM);

    //cout << src << endl;
    //cout << filename << " " << len << endl;
    fin.close();
}

void Scanner::getWord() {
    slen = 0;
    while (isalpha(src[pos]) || isdigit(src[pos]) || src[pos] == '_')
        sval[slen++] = src[pos++];
    sval[slen] = '\0';
}

TokenType Scanner::getToken() {
    while (isspace(src[pos]))
        pos++;

    if (isspace(src[pos])) {
        ;
    } else if (isalpha(src[pos]) || src[pos] == '_') { // op or label
        getWord();
        if (src[pos] == ':') {
            pos++;
            return TK_LABEL;
        } else
            return TK_ID;
    } else if (isdigit(src[pos]) || src[pos] == '-') { // number
        int sign;
        ival = 0;
        if (src[pos] == '-') { sign = -1; pos++; }
        else                   sign = 1;

        if (src[pos + 1] == 'x' || src[pos + 1] == 'X') {
            pos += 2;
            while (isdigit(src[pos]) || (src[pos] >= 'A' && src[pos] <= 'F')
                                 || (src[pos] >= 'a' && src[pos] <= 'f')) {
                if (isdigit(src[pos]))
                    ival = ival * 16 + src[pos++] - '0';
                else
                    ival = ival * 16 + toupper(src[pos++]) - 'A' + 10;
            }
        }
        else {
            while (isdigit(src[pos]))
                ival = ival * 10 + src[pos++] - '0';
        }
        ival *= sign;
        return TK_NUM;
    } else if (src[pos] == '\"') {  // string
        pos++;
        slen = 0;
        while (src[pos] != '\"') {
            if (src[pos] == '\\') {  // escape
                pos++;
                if (isdigit(src[pos])) { // oct
                    ival = 0;
                    while (src[pos] >= '0' && src[pos] <= '7')
                        ival = ival * 8 + src[pos++] - '0';
                    sval[slen++] = ival;
                }
                else if (src[pos] == 'x' || src[pos] == 'X' ) { // hex
                    printf("Fuck in hex!!\n");
                    printf("Fuck in hex!!\n");
                    printf("Fuck in hex!!\n");
                }
                else { // other
                    switch(src[pos++]) {
                        case '\\': sval[slen++] = '\\'; break;
                        case 'n' : sval[slen++] = '\n'; break;
                        case 't' : sval[slen++] = '\t'; break;
                        case '\"': sval[slen++] = '\"'; break;
                        case '\'': sval[slen++] = '\''; break;
                    }
                }
            } else
                sval[slen++] = src[pos++];
        }
        pos++;
        sval[slen] = '\0';
        return TK_STRING;
    } else if (src[pos] == '.') {  // assembler directives
        pos++;
        getWord();
        return TK_ASMDIR;
    } else if (src[pos] == '$') {  // register
        pos++;
        if (isdigit(src[pos])) {
            ival = 0;
            while (isdigit(src[pos]))
                ival = ival * 10 + src[pos++] - '0';
        } else {
            getWord();
            RegName *iter = std::lower_bound(regTable, regTable + REG_NUM,
                                                        sval);
            if (iter != regTable + REG_NUM && strcmp(iter->name,sval) == 0) 
                ival = iter->no;
            else
                printf("error in find register %s\n", sval);
        }

        return TK_REG;
    } else if (src[pos] == '#') {  // comment
        while (src[pos++] != '\n')
            ;
        return getToken();
    } else {
        switch (src[pos++]) {
            case ',' : return TK_COMMA;
            case '(' : return TK_RB_L;
            case ')' : return TK_RB_R;
            case '[' : return TK_SB_L;
            case ']' : return TK_SB_R;
            case '\0': return TK_EOF;
            default  : return TK_ERROR;
        }
    }
    return TK_ERROR;
}

int Scanner::getIntVal() { return ival; }
char *Scanner::getStrVal() { return sval; }

Scanner::~Scanner() {
    delete [] src;
}

bool Scanner::match(TokenType token, const char *errorMsg) {
    if (getToken() != token) {
        puts(errorMsg);
        return false;
    } else
        return true;
}

void scannerTest(const char *filename) {
    Scanner scanner(filename);
    int ct = 0;

    TokenType token;
    while ((token = scanner.getToken()) != TK_EOF) {
        printf("%2d: ", ct++);
        printf("%6s %4d %-6s\n", token2str[token], scanner.getIntVal(),
                                                   scanner.getStrVal());
        if (ct > 3)
            break;
    }
}
