#include "global.hpp"
#include "parser.hpp"
#include "simulator.hpp"
#include <cmath>
#include <algorithm>

const int MEM_SIZE = 1024 * 1024 * 8;

#define $sp 29
#define $ra 31
#define $v0 2
#define $a0 4
#define $a1 5
#define $lo 32
#define $hi 33

Simulator::Simulator(const char *filename, bool debugInfo, bool statsInfo) :
    parser(filename), text(parser.text), rawData(parser.data),
    labelTable(parser.labelTable), printDebug(debugInfo),printStats(statsInfo){
    // init environment
    mem = new unsigned char [MEM_SIZE];
    heapTop = 0;
    reg[$sp] = MEM_SIZE;

    pc = 0;
    halted = false;

    // pipeline stuff
    nop = Instruction {OP_NOP, {0, 0, 0}, 0, ""};

    // stats
    cycleCounter = insCounter = 0;
    loadCounter = storeCounter = 0;
    arithCounter = jumpCounter = 0;

    // deal data segment
    parser.parse();
    buildDataSegment();
    patchLabel();

    // init do instruction
    buildDoInstruction();
}

Simulator::~Simulator() {
    delete mem;
}

void Simulator::buildDataSegment() {
    vector<size_t> offsetRemap;

    // align and calc sum
    size_t sum = 0;
    for (size_t i = 0; i < rawData.size(); i++) {
        if (rawData[i].val == (unsigned char *)-1) { 
            int align = 1 << rawData[i].len;
            sum += (align - sum % align) % align;
        } else {
            sum += rawData[i].len;
        }
    }

    // copy to data segment
    unsigned char *data = mem;
    size_t offset = 0;
    for (size_t i = 0; i < rawData.size(); i++) {
        offsetRemap.push_back(offset);
        if (rawData[i].val == (unsigned char*)-1) {
            int align = 1 << rawData[i].len;
            offset += (align - offset % align) % align;
        } else {
            //cout << i <<" "<< rawData[i].len << " offset: " << offset <<endl;
            if (rawData[i].val)
                memcpy(data + offset, rawData[i].val, sizeof(char)
                                                    * rawData[i].len);
            offset += rawData[i].len;
        }
    }

    // remap offset
    for (auto iter = labelTable.begin(); iter != labelTable.end(); iter++) {
        if (iter->second.type == LABEL_DATA)
            iter->second.offset = offsetRemap[iter->second.offset];
    }
    heapTop = sum;

#if 0
    printf("===== data segment =====\n");
    for (size_t i = 0; i < heapTop; i++)
        cout << mem[i]; cout << endl;
    printf("===== data segment =====\n");
#endif
}
void Simulator::patchLabel() {
    for (size_t i = 0; i < text.size(); i++) {
        if (!text[i].label.empty()) {
            if (labelTable[text[i].label].type == LABEL_DATA) {
                text[i].op[1] = labelTable[text[i].label].offset;
                text[i].op[2] = 0; // $zero
            } else if (labelTable[text[i].label].type == LABEL_TEXT) {
                text[i].op[2] = labelTable[text[i].label].offset;
            }
        }
        if (text[i].cmd == OP_SYSCALL) {
            text[i].op[0] = i;
        } else if (text[i].cmd == OP_JAL) {
            text[i].op[1] = i;
        } else if (text[i].cmd == OP_DIV) {
            if (text[i].op[2] == 0)
                text[i].cmd = OP_DIV2;
        }
    }
}

#define Rdest      sm.reg[ins.op[0]]
#define Rsrc1      sm.reg[ins.op[1]]
#define Rsrc2      sm.reg[ins.op[2]]
#define RdestLock  sm.regLock[ins.op[0]]
#define Rsrc1Lock  sm.regLock[ins.op[1]]
#define Rsrc2Lock  sm.regLock[ins.op[2]]
#define resID      sm.resID
#define resEX      sm.resEX
#define resMEM     sm.resMEM

#define doID       doStage[(int)STAGE_ID]
#define doEX       doStage[(int)STAGE_EX]
#define doMEM      doStage[(int)STAGE_MEM]
#define doWB       doStage[(int)STAGE_WB]

void Simulator::buildDoInstruction() {
    // some constant
    static bool (* retTrue) (Instruction &ins, Simulator &sm);
    static bool (* notSupport) (Instruction &ins, Simulator &sm);
    static bool (* passMEM) (Instruction &ins, Simulator &sm);
    static bool (* passEX) (Instruction &ins, Simulator &sm);
    static bool (* addEX) (Instruction &ins, Simulator &sm);
    static bool (* simpleWB) (Instruction &ins, Simulator &sm);

    retTrue     = [](Instruction &ins, Simulator &sm) { return true; };
    passMEM     = [](Instruction &ins, Simulator &sm) {
                     resMEM[0] = resEX[0]; return true; };
    passEX      = [](Instruction &ins, Simulator &sm) {
                     resEX[0] = resID[0]; return true; };
    simpleWB    = [](Instruction &ins, Simulator &sm) {
                     Rdest = resMEM[0]; RdestLock--;  return true; };
    addEX       = [](Instruction &ins, Simulator &sm) {
                     resEX[0] = resID[0] + resID[1];  return true;};
    notSupport = [](Instruction &ins, Simulator &sm) {
                    cout << "unsupported instruciton" << endl;
                    sm.parser.printIns(ins);
                    exit(0);
                    return false; };
    // default
    for (int i = 0; i < INS_NUM; i++) {
        insItem[i].doID = notSupport;
        insItem[i].doEX = passEX;
        insItem[i].doMEM = passMEM;
        insItem[i].doWB = simpleWB;
    }

    /*** ABS ***/
    insItem[(int)OP_ABS].doID = [] (Instruction &ins, Simulator &sm) {
        if (Rsrc1Lock) return false;
        resID[0] = Rsrc1;  RdestLock++;  return true;
    };
    insItem[(int)OP_ABS].doEX = [] (Instruction &ins, Simulator &sm) {
        resEX[0] = abs(resID[0]); return true;
    };

    /*** NEG ***/
    insItem[(int)OP_NEG] = insItem[(int)OP_ABS];
    insItem[(int)OP_NEG].doEX = [] (Instruction &ins, Simulator &sm) {
        resEX[0] = -resID[0]; return true;
    };

    /*** ADD ***/
    insItem[(int)OP_ADD].doID = [] (Instruction &ins, Simulator &sm) {
        if (ins.regNum == 3) {
            if (Rsrc1Lock || Rsrc2Lock) return false;
            resID[0] = Rsrc1; resID[1] = Rsrc2;
        } else {
            if (Rsrc1Lock)              return false;
            resID[0] = Rsrc1; resID[1] = ins.op[2];
        }
        RdestLock++;  return true;
    };
    insItem[(int)OP_ADD].doEX = addEX;

#define BINARY_EXP(code, op) \
    insItem[(int)code] = insItem[(int)OP_ADD];\
    insItem[(int)code].doEX = [] (Instruction &ins, Simulator &sm) {\
        resEX[0] = (resID[0] op resID[1]); return true;\
    };

    /*** SUB ***/ /*** MUL ***/ /*** REM ***/ /*** XOR ***/
    BINARY_EXP(OP_SUB, -); BINARY_EXP(OP_MUL, *); BINARY_EXP(OP_REM, %);
    BINARY_EXP(OP_XOR, ^); BINARY_EXP(OP_DIV, /);

    /*** ADDU ***/
    insItem[(int)OP_ADDU] = insItem[(int)OP_ADD];
    /*** SUBU ***/
    insItem[(int)OP_SUBU] = insItem[(int)OP_SUB];

    /*** LI ***/
    insItem[(int)OP_LI].doID = [] (Instruction &ins, Simulator &sm) {
        resID[0] = ins.op[1]; RdestLock++; return true;
    };

    /*** DIV2 ***/
    insItem[(int)OP_DIV2].doID = [] (Instruction &ins, Simulator &sm) {
        if (RdestLock || Rsrc1Lock) return false;
        resID[0] = Rdest; resID[1] = Rsrc1;
        sm.regLock[$lo]++; sm.regLock[$hi]++; return true;
    };
    insItem[(int)OP_DIV2].doEX = [] (Instruction &ins, Simulator &sm) {
        resEX[0] = resID[0] / resID[1]; resEX[1] = resID[0] % resID[1];
        return true;
    };
    insItem[(int)OP_DIV2].doMEM = [] (Instruction &ins, Simulator &sm) {
        resMEM[0] = resEX[0]; resMEM[1] = resEX[1]; return true;
    };
    insItem[(int)OP_DIV2].doWB = [] (Instruction &ins, Simulator &sm) {
        sm.reg[$lo] = resMEM[0]; sm.reg[$hi] = resMEM[1];
        sm.regLock[$lo]--; sm.regLock[$hi]--;
        return true;
    };

    /*** B ***/
    insItem[(int)OP_B].doID = [] (Instruction &ins, Simulator &sm) {
        resID[0] = ins.op[2]; sm.jumpIn = true; return true;
    };
    insItem[(int)OP_B].doWB = [] (Instruction &ins, Simulator &sm) {
        sm.pc = resMEM[0];
        sm.jumpIn = false;  return true;
    };

    /*** BEQ ***/
    insItem[(int)OP_BEQ].doID = [] (Instruction &ins, Simulator &sm) {
        if (ins.regNum == 2) {
            if (RdestLock || Rsrc1Lock) return false;
            resID[0] = Rdest; resID[1] = Rsrc1;
        } else {
            if (RdestLock)              return false;
            resID[0] = Rdest; resID[1] = ins.op[1];
        }
        sm.jumpIn = true;  return true;
    };
    insItem[(int)OP_BEQ].doEX = [] (Instruction &ins, Simulator &sm) {
        resEX[0] = (resID[0] == resID[1]);  return true;
    };
    insItem[(int)OP_BEQ].doWB = [] (Instruction &ins, Simulator &sm) {
        if (resMEM[0]) sm.pc = ins.op[2]; // ins.op[2] should be passed
        sm.jumpIn = false;  return true;
    };

    /*** BEQZ ***/
    insItem[(int)OP_BEQZ] = insItem[(int)OP_BEQ];
    insItem[(int)OP_BEQZ].doID = [] (Instruction &ins, Simulator &sm) {
        if (RdestLock) return false;
        resID[0] = Rdest; sm.jumpIn = true; return true;
    };
    insItem[(int)OP_BEQZ].doEX = [] (Instruction &ins, Simulator &sm) {
        resEX[0] = (resID[0] == 0);  return true;
    };

#define CONDITION_BANCH(code, op) \
    insItem[(int)code] = insItem[(int)OP_BEQ];\
    insItem[(int)code].doEX = [] (Instruction &ins, Simulator &sm) {\
        resEX[0] = (resID[0] op resID[1]);  return true;\
    };

    CONDITION_BANCH(OP_BGE, >=);
    CONDITION_BANCH(OP_BGT, >);
    CONDITION_BANCH(OP_BLE, <=);
    CONDITION_BANCH(OP_BLT, <);
    CONDITION_BANCH(OP_BNE, !=);

#define CONDITION_BANCH_ZERO(code, op) \
    insItem[(int)code] = insItem[(int)OP_BEQZ];\
    insItem[(int)code].doEX = [] (Instruction &ins, Simulator &sm) {\
        resEX[0] = (resID[0] op 0);  return true;\
    };
 
    CONDITION_BANCH_ZERO(OP_BGEZ, >=); CONDITION_BANCH_ZERO(OP_BGTZ, >);
    CONDITION_BANCH_ZERO(OP_BLEZ, <=); CONDITION_BANCH_ZERO(OP_BLTZ, <);
    CONDITION_BANCH_ZERO(OP_BNEZ, !=);

    BINARY_EXP(OP_SLT, <);  BINARY_EXP(OP_SLE, <=);
    BINARY_EXP(OP_SGT, >);  BINARY_EXP(OP_SGE, >=);
    BINARY_EXP(OP_SEQ, ==); BINARY_EXP(OP_SNE, !=);

    BINARY_EXP(OP_SUB, -); BINARY_EXP(OP_MUL, *);
    BINARY_EXP(OP_REM, %); BINARY_EXP(OP_XOR, ^);


    /*** J ***/
    insItem[(int)OP_J] = insItem[(int)OP_B];
    /*** JR ***/
    insItem[(int)OP_JR].doID = [] (Instruction &ins, Simulator &sm) {
        if (RdestLock) return false;
        sm.jumpIn = true; resID[0] = Rdest; return true;
    };
    insItem[(int)OP_JR].doWB = [] (Instruction &ins, Simulator &sm) {
        sm.pc = resMEM[0]; sm.jumpIn = false; return true;
    };

    /*** JAL ***/
    insItem[(int)OP_JAL].doID = [] (Instruction &ins, Simulator &sm) {
        sm.jumpIn = true; sm.regLock[$ra]++; return true;
    };
    insItem[(int)OP_JAL].doWB = [] (Instruction &ins, Simulator &sm) {
        sm.reg[$ra] = ins.op[1] + 1; sm.pc = ins.op[2]; // should be passed
        sm.jumpIn = false; sm.regLock[$ra]--; return true;
    };

    /*** LA ***/
    insItem[(int)OP_LA].doID = [] (Instruction &ins, Simulator &sm) {
        if (Rsrc2Lock) return false;
        resID[0] = ins.op[1]; resID[1] = Rsrc2; RdestLock++; return true;
    };
    insItem[(int)OP_LA].doEX = addEX;

    /*** LW ***/
    insItem[(int)OP_LW].doID = [] (Instruction &ins, Simulator &sm) {
        if (Rsrc2Lock) return false;
        resID[0] = ins.op[1]; resID[1] = Rsrc2; RdestLock++; return true;
    };
    insItem[(int)OP_LW].doEX = addEX;
    insItem[(int)OP_LW].doMEM = [] (Instruction &ins, Simulator &sm) {
        if (sm.memLock) return false;
        sm.memLock = true; resMEM[0] = *((int *)(sm.mem + resEX[0]));
        return true;
    };

    /*** LB ***/
    insItem[(int)OP_LB] = insItem[(int)OP_LW];
    insItem[(int)OP_LB].doMEM = [] (Instruction &ins, Simulator &sm) {
        if (sm.memLock) return false;
        sm.memLock = true; resMEM[0] = *((char *)(sm.mem + resEX[0]));
        return true;
    };

    /*** SW ***/
    insItem[(int)OP_SW].doID = [] (Instruction &ins, Simulator &sm) {
        if (RdestLock || Rsrc2Lock) return false;
        resID[0] = ins.op[1]; resID[1] = Rsrc2; resID[2] = Rdest; return true;
    };
    insItem[(int)OP_SW].doEX =  [] (Instruction &ins, Simulator &sm) {
        resEX[0] = resID[0] + resID[1]; resEX[1] = resID[2]; return true;
    };
    insItem[(int)OP_SW].doMEM = [] (Instruction &ins, Simulator &sm) {
        if (sm.memLock) return false;
        sm.memLock = true; *((int *)(sm.mem + resEX[0])) = resEX[1];
        return true;
    };
    insItem[(int)OP_SW].doWB = retTrue;
    /*** SB ***/
    insItem[(int)OP_SB] = insItem[(int)OP_SW];
    insItem[(int)OP_SB].doMEM = [] (Instruction &ins, Simulator &sm) {
        if (sm.memLock) return false;
        sm.memLock = true; *((char *)(sm.mem + resEX[0])) = (char)resEX[1];
        return true;
    };
 
    /*** MOVE ***/
    insItem[(int)OP_MOVE].doID = [] (Instruction &ins, Simulator &sm) {
        if (Rsrc1Lock) return false;
        resID[0] = Rsrc1; RdestLock++; return true;
    };

    /*** MFLO ***/
    insItem[(int)OP_MFLO] = insItem[(int)OP_MOVE];
    insItem[(int)OP_MFLO].doID = [] (Instruction &ins, Simulator &sm) {
        if (sm.regLock[$lo]) return false;
        resID[0] = sm.reg[$lo]; RdestLock++; return true;
    };
    /*** MFHI ***/
    insItem[(int)OP_MFHI] = insItem[(int)OP_MOVE];
    insItem[(int)OP_MFHI].doID = [] (Instruction &ins, Simulator &sm) {
        if (sm.regLock[$hi]) return false;
        resID[0] = sm.reg[$hi]; RdestLock++; return true;
    };

    /*** NOP ***/
    insItem[(int)OP_NOP].doID = retTrue; insItem[(int)OP_NOP].doEX = retTrue;
    insItem[(int)OP_NOP].doMEM = retTrue; insItem[(int)OP_NOP].doWB = retTrue;

    /*** SYSCALL ***/
    insItem[(int)OP_SYSCALL].doID = [] (Instruction &ins, Simulator &sm) {
        if (sm.regLock[$v0] || sm.regLock[$a0] || sm.regLock[$a1])
            return false;
        sm.regLock[$v0]++;
        sm.jumpIn = true; return true;
    };
    insItem[(int)OP_SYSCALL].doEX = retTrue;
    insItem[(int)OP_SYSCALL].doMEM = retTrue;
    insItem[(int)OP_SYSCALL].doWB = [] (Instruction &ins, Simulator &sm) {
        sm.syscall(); sm.pc = ins.op[0] + 1; // go to next , should be passed
        sm.regLock[$v0]--;
        sm.jumpIn = false; return true;
    };
}

#define PIPELINE_ENABLE 1

void Simulator::run() {
    // call main
    pc = labelTable["main"].offset;

#if PIPELINE_ENABLE
    memset(stageIns, 0, sizeof(stageIns));
    while (!halted) {
        if (printDebug)
            printf(" --- pc: %2lu --- cycles: %2d ---\n", pc, cycleCounter);
        doCycle();
        cycleCounter++;
    }
#else
    while (!halted) {// && insCounter < 200){
        if (printDebug) {
            printf(" --- pc: %3lu --- ", pc);
            parser.printIns(text[pc]);
        }
        jumped = false;
        doInstruction(text[pc]);
        insCounter++;
        if (!jumped)
            pc++;
    }
#endif
    if (printDebug) cout << "********* halt *********   " << endl;
    if (printStats) {
        printf("Ins: %d  Cycle: %d\n\n", insCounter, cycleCounter);
    }
}

const char *stage2str[] = { "IF", "ID", "EX", "MEM", "WB" };

void Simulator::doCycle() {
    memLock = false;

    int i;
    for (i = STAGE_WB; i > STAGE_IF; i--) {
        bool res = true;
        if (stageIns[i] != NULL) {
            Instruction &ins = *stageIns[i];
            res = insItem[(int)ins.cmd].doStage[i](ins, *this);
        }

        if (i == STAGE_WB) {
            if (res && stageIns[i] != NULL && stageIns[i] != &nop)
                insCounter++;
        } else {
            if (res) {
                stageIns[i + 1] = stageIns[i];
                stageIns[i] = NULL;
            } else
                break;
        }
    }

    // IF
    if (i == STAGE_IF) {
        if (!memLock) {
            if (!jumpIn)   stageIns[STAGE_IF + 1] = &text[pc++];
            else           stageIns[STAGE_IF + 1] = &nop;
        }
    }

    if (printDebug) {
        for (int i = (int)STAGE_WB; i >= (int)STAGE_IF; i--) {
            printf("stage: %d ", i);
            if (stageIns[i] != NULL) parser.printIns(*stageIns[i]);
            else printf("    \n");
        }
        cout << endl;
    }
}

#undef Rdest
#undef Rsrc1
#undef Rsrc2
#define Rdest      reg[ins.op[0]]
#define Rsrc1      reg[ins.op[1]]
#define Rsrc2      reg[ins.op[2]]

void Simulator::doInstruction(Instruction &ins) {
#if 1
    memLock = false;
    size_t rawPc = pc;
    insItem[(int)ins.cmd].doID(ins, *this);
    insItem[(int)ins.cmd].doEX(ins, *this);
    insItem[(int)ins.cmd].doMEM(ins, *this);
    insItem[(int)ins.cmd].doWB(ins, *this);
    if (rawPc != pc) {
        jumped = true;
    }
#endif

#if 0
    int *op = ins.op; bool go;
    switch (ins.cmd) {
        case OP_ABS: Rdest = abs(Rsrc1);    break;
        case OP_ADD: if (ins.regNum == 2) Rdest = Rsrc1 + op[2];
                     else                 Rdest = Rsrc1 + Rsrc2;
                     break;
        case OP_ADDI: Rdest = Rsrc1 + op[2]; break;
        case OP_ADDU: if (ins.regNum == 2) // addi
                          Rdest =(int)((unsigned)Rsrc1 + (unsigned)op[2]);
                      else
                          Rdest =(int)((unsigned)Rsrc1 + (unsigned)Rsrc2);
                      break;
        case OP_ADDIU: Rdest = (int)((unsigned)Rsrc1 + (unsigned)op[2]); break;
        case OP_AND: if (ins.regNum == 2) Rdest = Rsrc1 & op[2];
                     else                 Rdest = Rsrc1 & Rsrc2;
                     break;
        case OP_ANDI: Rdest = Rsrc1 & op[2]; break;
        case OP_DIV: if (op[2] == 0) {
                         reg[$lo] = Rdest / Rsrc1; reg[$hi] = Rdest % Rsrc1;
                     } else {
                         if (ins.regNum == 2) { Rdest = Rsrc1 / op[2]; }
                         else { Rdest = Rsrc1 / Rsrc2; }
                     } break;
        case OP_DIVU: if (op[2] == 0) {
                          reg[$lo] = (int)((unsigned)Rdest / (unsigned)Rsrc1);
                          reg[$hi] = (int)((unsigned)Rdest % (unsigned)Rsrc1);
                      } else {
                          Rdest = (int)((unsigned)Rsrc1 / (unsigned)Rsrc2);
                      } break;
        case OP_MUL: if (ins.regNum == 2) Rdest = Rsrc1 * op[2];
                     else                 Rdest = Rsrc1 * Rsrc2;
                     break;
        //case OP_MULO: case OP_MULOU: case OP_MULT: case OP_MULTU:
        case OP_NEG: Rdest = -Rsrc1; break;
        //case OP_NEGU: case OP_NOR: case OP_NOT: case OP_OR: case OP_ORI:
        case OP_REM: if (ins.regNum == 2) { Rdest = Rsrc1 % op[2]; }
                     else                 { Rdest = Rsrc1 % Rsrc2; }
                     break;
        case OP_REMU: Rdest = (int)((unsigned)Rsrc1 % (unsigned)Rsrc2); break;
        //case OP_ROL: case OP_ROR: case OP_SLL: case OP_SLLV: case OP_SRA:
        //case OP_SRAV: case OP_SRL: case OP_SRLV:
        case OP_SUB: if (ins.regNum == 2) Rdest = Rsrc1 - op[2];
                     else                 Rdest = Rsrc1 - Rsrc2;
                     break;
        case OP_SUBU: if (ins.regNum == 2)
                          Rdest = (int)((unsigned)Rsrc1 - (unsigned)op[2]);
                      else
                          Rdest = (int)((unsigned)Rsrc1 - (unsigned)Rsrc2);
                      break;
        case OP_XOR: if (ins.regNum == 2) Rdest = Rsrc1 ^ op[2];
                     else                 Rdest = Rsrc1 ^ Rsrc2;
                     break;
        case OP_XORI: if (ins.regNum == 2) Rdest = Rsrc1 ^ op[2];
                      else                 Rdest = Rsrc1 ^ Rsrc2;
                      break;
        case OP_LI: Rdest = op[1]; break;
        //case OP_LUI:
        case OP_SEQ: if (ins.regNum == 2) {
                         Rdest = (Rsrc1 == op[2] ? 1 : 0);
                     } else {
                         Rdest = (Rsrc1 == Rsrc2 ? 1 : 0);
                     }
                     break;
        case OP_SGE: if (ins.regNum == 2) {
                         Rdest = (Rsrc1 >= op[2] ? 1 : 0);
                     } else {
                         Rdest = (Rsrc1 >= Rsrc2 ? 1 : 0);
                     }
                     break;
        case OP_SGT: if (ins.regNum == 2) {
                         Rdest = (Rsrc1 > op[2] ? 1 : 0);
                     } else {
                         Rdest = (Rsrc1 > Rsrc2 ? 1 : 0);
                     }
                     break;
        //case OP_SGTU: case OP_SLEU: case OP_SGEU:
        case OP_SLE:  if (ins.regNum == 2) {
                         Rdest = (Rsrc1 <= op[2] ? 1 : 0);
                     } else {
                         Rdest = (Rsrc1 <= Rsrc2 ? 1 : 0);
                     }
                     break;
        case OP_SLT: if (ins.regNum == 2) {
                         Rdest = (Rsrc1 < op[2] ? 1 : 0);
                     } else {
                         Rdest = (Rsrc1 < Rsrc2 ? 1 : 0);
                     }
                     break;
        //case OP_SLTI: case OP_SLTU: case OP_SLTIU:
        case OP_SNE: if (ins.regNum == 2) {
                         Rdest = (Rsrc1 != op[2] ? 1 : 0);
                     } else {
                         Rdest = (Rsrc1 != Rsrc2 ? 1 : 0);
                     }
                     break;
        case OP_B: pc = op[2]; jumped = true; break;
        case OP_BEQ: go = (ins.regNum == 2 ? Rdest == Rsrc1 : Rdest == op[1]);
                     if (go) { pc = op[2]; jumped = true; }
                     break;
        case OP_BEQZ: if (Rdest == 0) { pc = op[2]; jumped = true; }
                      break;
        case OP_BGE: go = (ins.regNum == 2 ? Rdest >= Rsrc1 : Rdest >= op[1]);
                     if (go) { pc = op[2]; jumped = true; }
                     break;
        //case OP_BGEU: 
        case OP_BGEZ: if (Rdest >= 0) { pc = op[2]; jumped = true; }
                      break;
        //case OP_BGEZAL:
        case OP_BGT: go = (ins.regNum == 2 ? Rdest > Rsrc1 : Rdest > op[1]);
                     if (go) { pc = op[2]; jumped = true; }
                     break;
        //case OP_BGTU:
        case OP_BGTZ: if (Rdest > 0) { pc = op[2]; jumped = true; }
                      break;
        case OP_BLE: go = (ins.regNum == 2 ? Rdest <= Rsrc1 : Rdest <= op[1]);
                     if (go) { pc = op[2]; jumped = true; }
                     break;
        //case OP_BLEU: case OP_BLEZ: case OP_BLTZAL:
        case OP_BLT: go = (ins.regNum == 2 ? Rdest < Rsrc1 : Rdest < op[1]);
                     if (go) { pc = op[2]; jumped = true; }
                     break;
        //case OP_BLTU: case OP_BLTZ:
        case OP_BNE: go = (ins.regNum == 2 ? Rdest != Rsrc1 : Rdest != op[1]);
                     if (go) { pc = op[2]; jumped = true; }
                     break;
        case OP_BNEZ: if (Rdest != 0) { pc = op[2]; jumped = true; }
                      break;
        case OP_J: pc = op[2];
                   jumped = true; break;
        case OP_JAL: reg[$ra] = pc + 1; pc = op[2];
                     jumped = true; break;
        case OP_JALR: reg[$ra] = pc + 1; pc = Rdest;
                     jumped = true; break;
        case OP_JR: pc = Rdest;
                    jumped = true;  break;
        case OP_LA: Rdest = op[1] + Rsrc2; break;
        //case OP_LB: Rdest = *((char *)(mem + calcOffset(ins))); break;
        case OP_LB: Rdest = *((char *)(mem + op[1] + Rsrc2)); break;
        //case OP_LBU: case OP_LD: case OP_LH: case OP_LHU:
        case OP_LW: Rdest = *((int *)(mem + op[1] + Rsrc2)); break;
        case OP_SB: *((char *)(mem + op[1] + Rsrc2)) = (char)Rdest; break;
        case OP_SW: *((int *)(mem + op[1] + Rsrc2)) = Rdest; break;
        //case OP_SD: case OP_SH:
        case OP_MOVE: Rdest = Rsrc1; break;
        case OP_MFHI: Rdest = reg[$hi]; break;
        case OP_MFLO: Rdest = reg[$lo]; break;
        case OP_NOP: break;
        case OP_SYSCALL: syscall(); break;
        default:
            printf("unsupported instruction\n");
            parser.printIns (ins);
            exit(0);
            break;
    }
#endif
}

void Simulator::syscall() {
    if (printDebug)
        cout << endl << "system call  " << reg[$v0] << endl;

    switch (reg[$v0]) {
        case 1: // print int
            printf("%d", reg[$a0]);       break;
        case 4:
            printf("%s", reg[$a0] + mem); break;
        case 5:
            scanf("%d", &reg[$v0]);         break;
        case 8:  // ignore $a1
            scanf("%s", reg[$a0] + mem);    break;
        case 9:
            heapTop += (4 - heapTop % 4) % 4;
            reg[$v0] = heapTop; heapTop += reg[$a0];
            break;
        case 10:
            halted = true;                  break;
        case 17:
            halted = true;                  break;
        default:
            printf("error in syscall\n");
    }
}

void simulatorTest(const char *filename, bool debugInfo = true,
                                         bool statsInfo = true) {
    Simulator simulator(filename, debugInfo, statsInfo);
    simulator.run();
}
