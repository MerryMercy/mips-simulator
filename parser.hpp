#ifndef __PARSER_HPP__
#define __PARSER_HPP__

#include "global.hpp"
#include "scanner.hpp"

struct Instruction {
    OpType cmd;
    int op[3];
    int regNum;
    string label;
};

struct DataBlock {
    unsigned char *val;
    size_t len;
};

typedef enum {LABEL_DATA, LABEL_TEXT} BlockType;
struct Label {
    BlockType type;
    size_t offset;
};

class Simulator;
class Parser {
    friend class Simulator;
public:
    Parser(const char *filename);
    ~Parser();

    void printIns(Instruction &ins);
    void printSelf();

private:
    void parse();
    Instruction getCommand();

    // store information 
    vector<Instruction> text;
    vector<DataBlock>   data;
    map<string, Label>  labelTable;

    TokenType token;
    Scanner scanner;
};

void parserTest(const char *filename);

#endif
